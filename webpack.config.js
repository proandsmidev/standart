const path = require('path'),
    HTMLWebpackPlugin = require('html-webpack-plugin'),
    NODE_ENV = process.env.NODE_ENV,
    IS_DEV = NODE_ENV === 'development',
    templatesPath = path.resolve(__dirname, './app/templates/'),
    templateType = 'default',
    autoprefixer = require('autoprefixer'),
    imageMin = require('imagemin-webpack'),
    { CleanWebpackPlugin } = require('clean-webpack-plugin'),
    setupDevTool = () => IS_DEV ? 'eval' : false;

module.exports = {
    mode: NODE_ENV ? NODE_ENV : 'development',
    entry: path.resolve(__dirname, './app/views/index.jsx'),
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'index.js'
    },
    resolve: {
        extensions: ['.jsx', '.js', '.json', '.tsx', '.ts'],
    },
    module: {
        rules: [
            {
                test: /\.[tj]sx?$/,
                use: ['ts-loader'],
            },
            {
                test: /\.s[ac]ss?$/,
                use: ['style-loader', {
                    loader: 'css-loader',
                    options: {
                        modules: {
                            mode: 'local',
                            localIdentName: '[name]__[local]--[hash:base64:5]',
                        }
                    }
                }, {
                    loader: 'postcss-loader',
                    options: {
                        plugins: [
                            autoprefixer(),
                        ],
                        sourceMap: true,
                    }
                }, 'sass-loader']
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader'
                    }
                ],
            }
        ],
    },
    plugins: [
        new imageMin({
            bail: false,
            cache: true,
            imageminOptions: {
                plugins: [
                    ['gifsicle', { interlaced: true }],
                    ['jpegtran', { progressive: true }],
                    ['optipng', { optimizationLevel: 5 }],
                    ['svgo', {
                        plugins: [
                            {
                                removeViewBox: false
                            }
                        ],
                    }]
                ],
            }
        }),
        new CleanWebpackPlugin(),
        new HTMLWebpackPlugin({
            filename: 'index.html',
            template: path.resolve(__dirname, templatesPath + '/' + templateType + '/_template.html'),
        })
    ],
    devServer: {
        port: 3000,
        open: true,
        hot: IS_DEV,
        noInfo: false,
        stats: 'errors-only',
    },
    devtool: setupDevTool(),
}