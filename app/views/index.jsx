import React from "react";
import ReactDOM from 'react-dom';

import { HeaderComponent } from '../components/header';
import { MainComponent } from '../components/main';
import { FooterComponent } from '../components/footer';

window.addEventListener('load', () => {
    ReactDOM.render(<HeaderComponent />, document.querySelector('.header'));

    ReactDOM.render(<MainComponent />, document.querySelector('.main'));

    ReactDOM.render(<FooterComponent />, document.querySelector('.footer'));
});

