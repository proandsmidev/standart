import React from "react";

import stylesFooter from '../sass/footer.sass';

export function FooterComponent() {
    return (
        <div className="container">
            <h1 className={stylesFooter.titleHeader}>
                Hello from footer!
            </h1>

            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus cumque illum laudantium maxime odio possimus praesentium rem rerum saepe voluptatum? Aperiam aspernatur at autem consequatur culpa cum delectus dolore dolorem earum eius enim eveniet exercitationem id inventore iure necessitatibus perferendis possimus quae, quas quod quos reprehenderit soluta sunt temporibus velit.
            </p>
        </div>
    );
}