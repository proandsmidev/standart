import React from "react";

import stylesHeader from '../sass/header.sass';

export function HeaderComponent() {
    return (
        <section>
            <h1 className={stylesHeader.example}>
                My component for header!
            </h1>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda beatae commodi eos, expedita id impedit labore nemo nisi nulla odio provident qui, saepe sapiente sint voluptatum. Blanditiis consequatur dignissimos dolore enim iure non perspiciatis repellat repellendus sint voluptatum? Accusamus aliquam blanditiis corporis delectus doloribus eius, eligendi est exercitationem incidunt nostrum officia quae, quod vel! Aliquid eaque exercitationem facilis, fugiat impedit laborum, necessitatibus numquam perferendis quas qui recusandae, totam. Aperiam aut beatae corporis cum dolor doloribus eaque et eum exercitationem expedita, facilis fuga fugit hic in incidunt magnam minus necessitatibus nesciunt nostrum obcaecati omnis quibusdam quisquam quod repellat rerum sed?
            </p>
        </section>
    );
}