import React from "react";

import stylesMain from '../sass/main.sass';

export function MainComponent() {
    return (
        <section>
            <h1 className={stylesMain.titleHeader}>
                Hello from main component!!
            </h1>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias aut commodi delectus distinctio eaque earum eligendi et eum ex exercitationem facilis, ipsa ipsam labore laudantium magnam maiores, nam nisi nobis odit officiis, praesentium quam quia rerum ut! Consequatur cumque exercitationem nulla optio voluptatum. Consectetur excepturi fuga illo nobis obcaecati vel, voluptatum! Aspernatur dolor in natus sit tempora. Alias architecto atque blanditiis culpa deleniti dignissimos dolore eius esse eum id minus modi, mollitia neque nobis nostrum odit optio praesentium provident quae, qui ratione rerum voluptas, voluptatem. A commodi consectetur dolorum minus molestias officiis omnis reiciendis sed suscipit voluptatibus! Asperiores aut cum doloribus eos est et eum exercitationem facere hic iure libero magnam, minima molestias necessitatibus non obcaecati officia officiis porro praesentium quia quibusdam quos, recusandae reprehenderit sint tempora temporibus tenetur vitae voluptas voluptatibus voluptatum? Alias delectus earum ipsam iure maxime minima nobis saepe sit velit. Assumenda autem cumque eius iusto nobis quibusdam voluptatem? Atque autem deserunt, dolor ex fugit laboriosam mollitia perspiciatis quasi repellat saepe voluptate voluptates? Ad alias aperiam blanditiis debitis dolor dolorem dolores eius, facilis iste iure laboriosam maiores nihil officia officiis optio pariatur placeat praesentium quae quaerat quia quibusdam quidem ratione repellat, sapiente sit soluta sunt tempora tempore, velit voluptatem? Deleniti dolore maiores numquam odio. Ab ad consectetur cumque deleniti deserunt ducimus ea error esse et facilis fuga fugiat id ipsa ipsam, minima, molestiae molestias nam natus nihil nisi nobis odit pariatur quam quasi quisquam reprehenderit voluptate! Cupiditate esse facere illum incidunt iure officiis qui quo sapiente.
            </p>
        </section>
    );
}